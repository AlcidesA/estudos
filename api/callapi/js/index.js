var form = document.querySelector("#callApi");


form.addEventListener('click', function() {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8888/estudos/api/api/product/read.php");

    xhr.addEventListener("load", function() {

        if (xhr.status == 200) {
            console.log('sucesso');
            var resposta = xhr.responseText;
            var records = JSON.parse(resposta);

            records.records.forEach(function(record) {
                console.log(record);
                adicionaRecord(record);
            });

        } else {
            console.log('erro');
        }
    });

    xhr.send();

})


function adicionaRecord(record) {
    var recordTr = montaTr(record);
    var tbody = document.getElementById('api_tbody');
    tbody.appendChild(recordTr);

}

function montaTr(record) {
    var recordTr = document.createElement("tr");
    recordTr.classList.add("record");

    recordTr.appendChild(montaTd(record.category_id, "category_id"));
    recordTr.appendChild(montaTd(record.category_name, "category_name"));
    recordTr.appendChild(montaTd(record.description, "description"));
    recordTr.appendChild(montaTd(record.id, "id"));
    recordTr.appendChild(montaTd(record.name, "name"));
    recordTr.appendChild(montaTd(record.price, "price"));

    return recordTr;
}

function montaTd(dado, classe) {
    var td = document.createElement("td");
    td.classList.add(classe);

    td.textContent = dado;

    return td;

}