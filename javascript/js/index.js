// ES6

//valor default caso não seja passado Y
const multiply = (x, y = 1) => {
    return x * y
}

//pegando todos os parametros de uma função com ... (rest params)
function sum(...numbers) {
    let result = 0
    numbers.forEach((number) => {
      result += number
    })
    return result
}

sum(1, 2, 3, 4, 5) // 15


//destructuring - declarando variaveis
const [a, b] = [1, 2]

console.log(a) // 1
console.log(b) // 2

// e com rest params 

const [a, b, ...rest] = [1, 2, 3, 4, 5]

console.log(a) // 1
console.log(b) // 2
console.log(rest) // 3, 4, 5

//com objetos
const person = { name: 'Matheus', age: 26 }

const {name, age} = person

console.log(name) // 'Matheus'
console.log(age) // 26


// import de libs 

// old 
var ReactRouter = require('react-router');
var Route = ReactRouter.Route;
var Link = ReactRouter.Link;
var Router = ReactRouter.Router;

//new ES6

const { 
    Route, 
    Link, 
    Router
  } = require('react-router')


  /////


// .map ES6 para pegar o quadrado de cada numero
const numbers = [1, 2, 3];
const square = x => x * x;
const squaredNumbers = numbers.map(square); // [1, 4, 9]


// .filter com ES6 para numeros maiores que 4
var numbers = [1, 4, 7, 10];

var isBiggerThanFour = function(value) {
    return value > 4;
};

var numbersBiggerThanFour = numbers.filter(isBiggerThanFour); // [7, 10]