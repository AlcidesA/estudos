import React, {Component} from 'react';

class Clock extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            date: new Date()
        }

        // this.handleSetDate = this.handleSetDate.bind(this);

    }

    componentDidMount(){
        setInterval(() => {
            this.setState({date: new Date()})
        }, 1000);
    }

    // handleSetDate(e){
    //     e.preventDefault();
    //     this.setState({ date: new Date() })
    // }
  

    render(){

        return(
            <div>
                <h1>{this.state.date.toLocaleTimeString()}</h1>
                {/* <a onClick={this.handleSetDate} >Atualizar</a> */}
                {/* <a onClick={this.handleSetDate.bind(this)} >Atualizar</a> */}
            </div>
        )
    }


}

export default Clock;