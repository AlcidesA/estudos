import dispatcher from '../dispatcher';

export function createTodo(text){
    dispatcher.dispatch({
        type:'CREATE_TODO',
        text
    });
}

export function deleteTodo(id){
    dispatcher.dispatch({
        type:'DELETE_TODO'
    });
}

export function reloadTodos(){
    // axios('http://someurl.com/dataendpoint').then( (data) => {
    //     console.log('got the data!', data)
    // });

    dispatcher.dispatch({ type:'FETCH_TODOS' });

    setTimeout( () => {
        dispatcher.dispatch({ type:'RECEIVE_TODOS', todos: [{
                id: 3,
                text: 'Sleep Early Again',
                complete:false
            },
            {
                id: 4,
                text: 'Read Again',
                complete:false
            }
        ]});
        // para capturar um erro
        // if(false){
        //     dispatcher.dispatch({ type:'FETCH_TODOS_ERROR' });
        // }
    }, 1500);
}