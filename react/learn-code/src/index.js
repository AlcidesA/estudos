import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './pages/Home';
import Sobre from './pages/Sobre';
import Todo from './pages/Todo';

import { BrowserRouter, Switch, Route } from 'react-router-dom';


// ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={App} />
            <Route path="/sobre" component={Sobre} />
            <Route path="/todo" component={Todo} />
            
        </Switch>
    </ BrowserRouter>
    , document.getElementById('root'));


// registerServiceWorker();
 
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();

// -----------------------SIMPLE REDUX-----------------------
// import { createStore } from 'redux';

// const reducer = (state,action) => {
//     if(action.type === 'INC'){
//         return state+action.payload;
//     }else if(action.type === 'DEC'){
//         return state-action.payload;
//     }else if(action.type === 'E'){
//         throw new Error('Erro manual!');
//     }
//     return state;
// }

// const logger = (store) => (next) => (action) => {
//     console.log('action fired',action);
//     next(action);
// }

// const error = (store) => (next) => (action) => {
//     try{
//         next(action);
//     } catch(e){
//         console.log('errorrr', e)
//     }
// }

// const middleware = applyMiddleware(logger, error);

// const store = createStore( reducer, 0, middleware);

// store.subscribe(() =>{
//     console.log('store changed', store.getState());
// });

// store.dispatch({type: 'INC',payload: 1});
// store.dispatch({type: 'INC',payload: 2});
// store.dispatch({type: 'INC',payload: 3});
// store.dispatch({type: 'DEC',payload: 1});
// store.dispatch({type: 'E'});



// REDUX

// const userReducer = (state={},action) => {
//     switch(action.type){
//         case "CHANGE_NAME":{
//             state = {...state, name:action.payload}
//             break
//         }
//         case "CHANGE_AGE":{
//             state = {...state, age:action.payload}
//             break
//         }
//     }
    
//     return state;
// };

// const tweetsReducer = (state=[],action) => {

//     return state;
// };

// const reducers = combineReducers({
//     user: userReducer,
//     tweets:tweetsReducer
// })

// const store = createStore(reducers,{
//     user:{
//         name:'alcides',
//         age:21
//     },
//     tweets:[]
// });

// store.subscribe(() =>{
//     console.log('store changed', store.getState());
// });

// store.dispatch({type: 'CHANGE_NAME',payload: 'Alcides Augusto'});
// store.dispatch({type: 'CHANGE_AGE',payload: 211});

// REDUX WITH ASYNC

// import logger from 'redux-logger';
// import axios from 'axios';
// import thunk from 'redux-thunk';
// import promise from 'redux-promise-middleware';

//WITHOUT PROMISSE

// const initialState = {
//     fetching: false,
//     fetched:false,
//     data: [],
//     error:null
// }

// const reducer = ( state=initialState, action) => {
//     switch(action.type){
//         case 'FETCH_USERS_START':{
//             return {...state,fetching:true}
//             break;
//         }
//         case 'FFETCH_USERS_ERROR':{
//             return {...state, fetching:false, error:action.payload}
//             break;
//         }
//         case 'RECEIVE_USERS':{
//             return {...state, fetching:false, fetched:true, data:action.payload} 
//             break;
//         }
//     }

//     return state;
// }

// const middleware = applyMiddleware(thunk, logger);
// const store = createStore(reducer,middleware);


// store.dispatch((dispatch) => {
//     dispatch({type:'FETCH_USERS_START'})
//     axios.get('https://jsonplaceholder.typicode.com/todos/1')
//     .then((response) => {
//         dispatch({type:'RECEIVE_USERS',payload:response.data})
//     })
//     .catch((err) => {
//         dispatch({type:'FETCH_USERS_ERROR',payload:err})
//     })
    
// });


//WITH PROMISSE

// const initialState = {
//     fetching: false,
//     fetched:false,
//     data: [],
//     error:null
// }

// const reducer = ( state=initialState, action) => {
//     switch(action.type){
//         case 'FETCH_USERS_PENDING':{
//             return {...state,fetching:true}
//             break;
//         }
//         case 'FETCH_USERS_REJECTED':{
//             return {...state, fetching:false, error:action.payload}
//             break;
//         }
//         case 'FETCH_USERS_FULLFILLED':{
//             return {...state, fetching:false, fetched:true, data:action.payload} 
//             break;
//         }
//     }

//     return state;
// }

// const middleware = applyMiddleware(promise(), thunk, logger);
// const store = createStore(reducer,middleware);



// store.dispatch({
//     type:'FETCH_USERS',
//     payload: axios.get('https://jsonplaceholder.typicode.com/todos/1')
// });