import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import logo from './logo.svg';
// import './App.css';

class App extends Component {
  render() {
    return (
      <div className="home">
        <header className="App-header">
            <h1>Home</h1>
            <Link to="/sobre?oi=opa?filter=none">Página sobre</Link> 
            <Link to="/todo">Página todo</Link> 
        </header>
      </div>
    );
  }
}

export default App;
