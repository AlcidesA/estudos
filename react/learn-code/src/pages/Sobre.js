import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import logo from './logo.svg';
// import './App.css';


class Sobre extends Component {

  render() {
    return (
      <div className="sobre">
        <header className="App-header">
            <h1>Sobre</h1>
            <Link to="/">Página home</Link> 
            <Link to="/todo">Página todo</Link> 
        </header>
      </div>  
    );
  }
}

export default Sobre;
