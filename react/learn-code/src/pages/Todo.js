import React, { Component } from 'react';
import * as TodoActions from '../actions/TodoActions'
import TodoStore from '../store/TodoStore';
import { Link } from 'react-router-dom';



class Todo extends Component {

    constructor(){
        super();

        this.getTodos = this.getTodos.bind(this);
        this.state = {
            todos: TodoStore.getAll()
        }
    }


    componentWillMount(){
        TodoStore.on('change', this.getTodos);
        console.log('count', TodoStore.listenerCount('change'));
    }

    componentWillUnmount(){
        TodoStore.removeListener('change',this.getTodos);
    }

    createTodo(){
        TodoActions.createTodo(Date.now());
    }

    getTodos(){
        this.setState({
            todos:TodoStore.getAll(),
        });
    }

    reloadTodos(){
        TodoActions.reloadTodos();
    }

    render() {
        const { todos } = this.state;
        console.log(todos);
        const TodoComponents = todos.map((todo) => {
            return <li key={todo.id}>{todo.text}</li>;
        })

        return (
            <div>
                <Link to="/">Página home</Link> 
                <button onClick={ this.createTodo.bind(this) }>Create!</button>
                <button onClick={ this.reloadTodos.bind(this) }>Reload!</button>
                <h1>Todos</h1>
                <ul>{TodoComponents}</ul>
            </div>
        );
    }
}

export default Todo;