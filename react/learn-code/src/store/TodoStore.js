import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class TodoStore extends EventEmitter{
    constructor(){
        super()

        this.todos = [
            {
                id: 1,
                text: 'Sleep Early',
                complete:false
            },
            {
                id: 2,
                text: 'Read ',
                complete:false
            }
        ]

    }

    createTodo(text){
        const id = Date.now();
        
        this.todos.push({
            id,
            text,
            complete:false
        });

        this.emit('change');
    }

    

    getAll() {
        return this.todos;
    }

    handleActions(action){
        console.log('TodoStore received an action', action);
        switch(action.type){
            case 'CREATE_TODO':{
                this.createTodo(action.text);
                break;
            }
            case 'RECEIVE_TODOS':{
                this.todos = action.todos;
                this.emit('change');
                break;
            }
            default:{
                console.log('default');
            }
        }
    }   
    
}

const todoStore = new TodoStore();

// window.todoStore = todoStore;
window.dispatcher = dispatcher;

dispatcher.register(todoStore.handleActions.bind(todoStore)); 

export default todoStore;